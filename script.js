let searchParams = new URLSearchParams(window.location.search)
var innerForm = $("#innerForm");

var jqXHR = $.get(searchParams.get('quiz'), function(data, status) {
  window.quiz = data;
  main();
});
var quiz = window.quiz; //hack

function makeAnswerstring(answers, id) {
  var completeanswerstring = "";
  for (let answer = 0; answer < answers.length; answer++) {
    const element = answers[answer];
    var answerstring = `
    <div class="custom-control custom-radio">
    <input data-correct="${element["correct"]}" type="radio" class="custom-control-input required" id="${element["shortname"]}" name="${id}" value="${element["shortname"]}">
    <label class="custom-control-label" for="${element["shortname"]}">${element["text"]}</label>
    </div>
    `;
    var completeanswerstring = completeanswerstring + answerstring;
  }
  innerForm.append(completeanswerstring);
}

function main() {
  $("body").css("background-image", `url(${window.quiz["bg-image"]})`);
  $("#title")[0].innerHTML = window.quiz["title"];
  for (let question = 0; question < quiz["questions"].length; question++) {
    const element = quiz["questions"][question];
    innerForm.append(`<br><h2>${element["question"]}</h2>`);
    innerForm.append(makeAnswerstring(element["answers"], question));
  }
  innerForm.append(
    `<br><button onclick="checkresults()" type="button" class="btn btn-primary">Check</button>`
  );
}

var points = 0;
function checkresults() {
  for (let question = 0; question < quiz["questions"].length; question++) {
    const element = quiz["questions"][question];
    var radios = document.getElementsByName(question);
    if (radios === undefined) {
      return;
    }
    for (var i = 0, length = radios.length; i < length; i++) {
      if (radios[i].checked) {
        var correctvalues = [];
        for (
          let answer = 0;
          answer < quiz["questions"][question]["answers"].length;
          answer++
        ) {
          const element = quiz["questions"][question]["answers"][answer];
          if (element.correct === true) {
            correctvalues.push(element.shortname);
          }
        }
        // do whatever you want with the checked radio
        if ($.inArray(radios[i].value, correctvalues) != -1) {
          points = points + 1;
        }
        // only one radio can be logically checked, don't check the rest
        break;
      }
    }
  }
  var number_of_questions = quiz["questions"].length;
  var percent = (points / number_of_questions) * 100;
  if (percent < 40) {
    swal("Dumb", `That was pretty bad!  Your score: ${percent + "%"}`, "error");
  } else if (percent <= 50) {
    swal("Dumb", `Not very good.   Your score: ${percent + "%"}`, "warning");
  } else if (percent <= 75) {
    swal(
      "You are not very dumb",
      `Could be better.   Your score: ${percent + "%"}`,
      "warning"
    );
  } else if (percent < 100) {
    swal(
      "Very good",
      `Close to perfect!   Your score: ${percent + "%"}`,
      "success"
    );
  } else if (percent === 100) {
    swal(
      "Perfect",
      `You got them all right!   Your score: ${percent + "%"}`,
      "success"
    );
  }
  $("#innerForm").remove();
  $(".outerForm")[0].innerHTML =
    "You did this Quiz already. Refresh the page to do it again.";
}
